
/*----------------------------------------------------------------
Asset manager to load Image by name
*/
function ASSET_GetImageFromName(name) {
    let img=null;

    ASSET_DBC.forEach(ast=>{
        if ( (ast.name == name) && (ast.type == "image") )
        img=ast.data;
    })

    return img;
}

/*----------------------------------------------------------------
Asset manager to load sound by name
*/
function ASSET_GetSoundFromName(name) {
    let img=null;

    ASSET_DBC.forEach(ast=>{
        if ( (ast.name == name) && (ast.type == "sound") )
        img=ast.data;
    })

    return img;
}

/*----------------------------------------------------------------
Asset manager to load font by name
*/
function ASSET_GetFontFromName(name) {
    let img=null;

    ASSET_DBC.forEach(ast=>{
        if ( (ast.name == name) && (ast.type == "font") )
        img=ast.data;
    })

    return img;
}

/*----------------------------------------------------------------
Asset manager to load font by name
*/
function ASSET_GetJSONFromName(name) {
    let img=null;

    ASSET_DBC.forEach(ast=>{
        if ( (ast.name == name) && (ast.type == "json") )
        img=ast.data;
    })

    return img;
}

function ASSET_INIT() {
    soundFormats('mp3', 'ogg');

    ASSET_DBC.forEach(ast=>{
        ast.data = ast.loader();
    });
}