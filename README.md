# CGDC GLOBAL GAME JAM 2022 #

This is a game engine based on JSON project. The purpose is to have a game that can be designed just using
assets and one json file.
The main game is based on story not a lots of animation so we focus the work on the designer (Script writer, story)
there will be as well a puzzle to solve

### What is this repository for? ###

* Separate work between dev and design
* Version control

### How to work
***
This file is main core of the game : **test.json**
```json
{
  "Games": 
  [
    {
      "LevelName":"Main Menu",
      "Dialog":["Click to Start"],
      "Puzzle":null,
      "NextScene":"Intro"
    },
    {
      "LevelName":"Intro",
      "Music":"innerworldmusic",
      "Player":"char1",
      "X":200,
      "Y":20,
      "Scale":0.25,
      "Dialog":["Hello","Byebye"],
      "Puzzle":null,
      "NextScene":"Main Menu"
    }
  ]
}
```
* LevelName : <p> is use to tag each scene and also use to link the scene between them </p>
* NextScene : <p> Put here the scene to load when the dialog is finished 
(Need to be liked with the result of a puzzle)</p>
* Dialog : <p> ["aeraera","aetaetat",] : </p>
<p> Add as many as dialog you want (May be we can include some more option </>
<p> May be we can have a descrptor inside the dialog
```json
...
"Dialog":[
{
"actor":"Player"
 "text":"Hello aevaeoviajoij..."
 "Picture":"faceplayer"
 "action":"click"
},
{
"actor":"Boss"
 "text":"aeaerar aevaeoviajoij..."
 "Picture":"faceboss"
 "action":"click"
}
]
```
### NEXT ###

* Designer can add there requirement regarding the functionnaly but they have to specific the JSON format for that.
<p> ***Example:***</p>
```json
Play a sound if the user click 
"UserAction" : [
{
"event":"mouseclick",
"action":"playmusic",
"music":"gameover"
}]
```