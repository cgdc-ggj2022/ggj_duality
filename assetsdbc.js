var DialogManager=null;

let ASSET_DBC=[
    {
      name:"font1",
      type:"font",
      loader : function() {return loadFont('assets/freefont.ttf');},
      data:null
    },
    {
      name:"sequence",
      type:"json",
      loader : function() {return loadJSON('assets/sequence.json');},
      data:null
    },
    {
      name:"game",
      type:"json",
      loader : function() {return loadJSON('assets/game.json');},
      data:null
    },
    {
      name:"introingame",
      type:"json",
      loader : function() {return loadJSON('assets/introinnerworld.json');},
      data:null
    },
    {
      name:"ingameSP",
      type:"json",
      loader : function() {return loadJSON('assets/ingame.json');},
      data:null
    },
    {
      name:"ingameSP1",
      type:"json",
      loader : function() {return loadJSON('assets/ingame1.json');},
      data:null
    },
    {
      name:"ingameSP2",
      type:"json",
      loader : function() {return loadJSON('assets/ingame2.json');},
      data:null
    },
    {
      name:"ingameSP3",
      type:"json",
      loader : function() {return loadJSON('assets/ingame3.json');},
      data:null
    },
    {
      name:"ingameSP4",
      type:"json",
      loader : function() {return loadJSON('assets/ingame4.json');},
      data:null
    },
    {
      name:"ingameSP5",
      type:"json",
      loader : function() {return loadJSON('assets/ingame5.json');},
      data:null
    },
    {
      name:"ingameSP6",
      type:"json",
      loader : function() {return loadJSON('assets/ingame6.json');},
      data:null
    },
    {
      name:"ingameSP7",
      type:"json",
      loader : function() {return loadJSON('assets/ingame7.json');},
      data:null
    },
    {
      name:"fleshgame",
      type:"json",
      loader : function() {return loadJSON('assets/fleshgame.json');},
      data:null
    },
    {
      name:"fleshgame1",
      type:"json",
      loader : function() {return loadJSON('assets/fleshgame1.json');},
      data:null
    },
    {
      name:"fleshgame2",
      type:"json",
      loader : function() {return loadJSON('assets/fleshgame2.json');},
      data:null
    },
    {
      name:"fleshgame3",
      type:"json",
      loader : function() {return loadJSON('assets/fleshgame3.json');},
      data:null
    },
    {
      name:"fleshgame4",
      type:"json",
      loader : function() {return loadJSON('assets/fleshgame4.json');},
      data:null
    },
    {
      name:"fleshgame5",
      type:"json",
      loader : function() {return loadJSON('assets/fleshgame5.json');},
      data:null
    },
    {
      name:"fleshgame6",
      type:"json",
      loader : function() {return loadJSON('assets/fleshgame6.json');},
      data:null
    },
    {
      name:"fleshgame7",
      type:"json",
      loader : function() {return loadJSON('assets/fleshgame7.json');},
      data:null
    },
    {
      name:"gameover",
      type:"json",
      loader : function() {return loadJSON('assets/gameover.json');},
      data:null
    },
    {
      name:"wingame",
      type:"json",
      loader : function() {return loadJSON('assets/wingame.json');},
      data:null
    },
    {
      name:"hero1",
      type:"image",
      loader : function() {return loadImage('assets/hero_1.png');},
      data:null
    },
    {
      name:"hero2",
      type:"image",
      loader : function() {return loadImage('assets/hero_2.png');},
      data:null
    },
    {
      name:"boss1",
      type:"image",
      loader : function() {return loadImage('assets/boss_1.png');},
      data:null
    },
    {
      name:"boss2",
      type:"image",
      loader : function() {return loadImage('assets/boss_2.png');},
      data:null
    },
    {
      name:"backgroundfight1",
      type:"image",
      loader : function() {return loadImage('assets/officeflesh.png');},
      data:null
    },
    {
      name:"backgroundfight2",
      type:"image",
      loader : function() {return loadImage('assets/officespirit.png');},
      data:null
    },
    {
      name:"mouse",
      type:"image",
      loader : function() {return loadImage('assets/mouse.png');},
      data:null
    },
    {
      name:"messagebox",
      type:"image",
      loader : function() {return loadImage('assets/messagebox.png');},
      data:null
    },
    {
      name:"pow",
      type:"image",
      loader : function() {return loadImage('assets/pow.png');},
      data:null
    },
    {
      name:"bookintro1",
      type:"image",
      loader : function() {return loadImage('assets/bookinto1.png');},
      data:null
    },
    {
      name:"bookintro2",
      type:"image",
      loader : function() {return loadImage('assets/bookinto2.png');},
      data:null
    },
    {
      name:"bookintro3",
      type:"image",
      loader : function() {return loadImage('assets/bookinto3.png');},
      data:null
    },
    {
      name:"bookintro4",
      type:"image",
      loader : function() {return loadImage('assets/bookinto4.png');},
      data:null
    },
    {
      name:"bookintro5",
      type:"image",
      loader : function() {return loadImage('assets/bookinto5.png');},
      data:null
    },
    {
        name:"layer1",
        type:"sound",
        loader : function() {return loadSound('assets/Innerworld_LAYER1_MAIN.mp3');},
        data:null
    },
    {
        name:"layer2",
        type:"sound",
        loader : function() {return loadSound('assets/Innerworld_LAYER2_SPIRIT.mp3');},
        data:null
    },
    {
        name:"layer3",
        type:"sound",
        loader : function() {return loadSound('assets/Innerworld_LAYER3_FLESH.mp3');},
        data:null
    },
    {
        name:"overworld",
        type:"sound",
        loader : function() {return loadSound('assets/Overworld.mp3');},
        data:null
    },
    {
        name:"SFX_Damage",
        type:"sound",
        loader : function() {return loadSound('assets/SFX_Damage.mp3');},
        data:null
    },
    {
        name:"SFX_Failure",
        type:"sound",
        loader : function() {return loadSound('assets/SFX_Failure.mp3');},
        data:null
    },
    {
        name:"SFX_Dialogue",
        type:"sound",
        loader : function() {return loadSound('assets/SFX_Dialogue.mp3');},
        data:null
    },
    {
        name:"SFX_Success",
        type:"sound",
        loader : function() {return loadSound('assets/SFX_Success.mp3');},
        data:null
    },
    {
        name:"SFX_Punch",
        type:"sound",
        loader : function() {return loadSound('assets/SFX_Punch.mp3');},
        data:null
    }
    
    ];