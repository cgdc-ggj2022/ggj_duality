class Message {
    constructor(json)
    {
        this.sequence=json.sequence;
        this.idx=0;
        this.counter=0;
        this.messageindex=0;
        this.window=null;
        this.background = ASSET_GetImageFromName(this.sequence[this.idx].background);
        this.x = 0;
        this.y = 400;
        this.sx = 0.75;
        this.sy = 0.75;
        this.line1=null;

        if (this.sequence[this.idx].wait != undefined)
            this.displaytime = this.sequence[this.idx].wait/0.016;
        else
            this.displaytime = 100000000000000;



        if (this.sequence[this.idx].X != undefined)
        this.x = this.sequence[this.idx].X;
        if (this.sequence[this.idx].Y != undefined)
        this.y = this.sequence[this.idx].Y;
        if (this.sequence[this.idx].SX != undefined)
        this.sx = this.sequence[this.idx].SX;
        if (this.sequence[this.idx].SY != undefined)
        this.sy = this.sequence[this.idx].SY;

        if (this.sequence[this.idx].window != undefined) {
            this.window = ASSET_GetImageFromName(this.sequence[this.idx].window);
        }
        else if (this.sequence[this.idx].window == "NONE")
        {

        }
        else
            this.window = ASSET_GetImageFromName("messagebox")


        if (this.sequence[this.idx].line1 != undefined)
        this.line1 = this.sequence[this.idx].line1;
        if (this.sequence[this.idx].line2 != undefined)
        this.line2 = this.sequence[this.idx].line2;
        if (this.sequence[this.idx].line3 != undefined)
        this.line3 = this.sequence[this.idx].line3;

        this.dialogueplaying = false;



        DialogManager = this;
    }

    update()
    {
        this.counter++;
        if (this.line1 != null)
        if (this.dialogueplaying == false)
        {
            this.dialogueplaying = true;
            GM_PLAY_DIALOG();
        }
        if (this.counter>this.displaytime)
        {
            this.dialogueplaying = false;
            this.counter=0;
            this.idx++;
            if (this.idx>=this.sequence.length)
            {
                DialogManager=null;
                //GM_STOP_DIALOG();
            }
            else
            {
                this.counter=0;
                this.messageindex=0;
                if (this.sequence[this.idx].wait != undefined)
                this.displaytime = this.sequence[this.idx].wait/0.016;
                else
                this.displaytime = 100000000000000;
                this.window=null;
                this.background=null;
                if (this.sequence[this.idx].background!=undefined)
                this.background = ASSET_GetImageFromName(this.sequence[this.idx].background);
                if (this.sequence[this.idx].window != undefined) {
                    this.window = ASSET_GetImageFromName(this.sequence[this.idx].window);
                }
                else
                    this.window = ASSET_GetImageFromName("messagebox")

                if (this.sequence[this.idx].line1 != undefined)
                this.line1 = this.sequence[this.idx].line1;
                if (this.sequence[this.idx].line2 != undefined)
                this.line2 = this.sequence[this.idx].line2;
                if (this.sequence[this.idx].line3 != undefined)
                this.line3 = this.sequence[this.idx].line3;
            }
        }
    }

    draw()
    {
        
        let scx = 0.75;
        let scy = 0.35;
        scale(scx,scy);
        if (this.window!=null)
        image(this.window,0,0);
        scale(1/scx,1/scy);
        fill(255);
        textSize(18);
        textAlign(LEFT);
        if (this.sequence[this.idx].line1 != undefined)
        text(this.line1,250,10);
        if (this.sequence[this.idx].line2 != undefined)
        text(this.line2,250,40);
        if (this.sequence[this.idx].line3 != undefined)
        text(this.line3,250,70);

        scx = this.sx;
        scy = this.sy;
        scale(scx,scy);
        if (this.background !=null)
        image(this.background,this.x,this.y);
        scale(1/scx,1/scy);

    }

    next() {
        this.counter=this.displaytime;
    }
}