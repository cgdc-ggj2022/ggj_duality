class Particle
{
  constructor(x=0,y=0,color=255)
  {
     this.x=x;
     this.y=y;
     this.color = color;
     this.speedx=Math.random()*8-4;
     this.speedy=Math.random()*8-4;
     this.lifetime = Math.random()*64+8;
  }

  update()
  {
    this.x+=this.speedx;
    this.y+=this.speedy;
    if (this.lifetime>0)
    this.lifetime--;

  }

  draw()
  {
    fill(this.color,this.lifetime*5);
    rect(this.x,this.y,this.lifetime*0.25,this.lifetime*0.25)
  }
}


class PillarX
  {
    constructor(x=0,y=0,w=300,h=300,speed=1)
    {
      this.sy = y;
      this.sx = x;
      this.sw = w;
      this.sh = h;

      this.x = x;
      this.py = h/2;
      this.speed=speed;
      this.pyspeed=2;
      this.pyacc=0;
      this.gap = 120;
      this.w = w-50 ;
      this.h = h-50 ;
      this.posY=10;//Math.random()*(this.h/2-50)+25;
      this.reverse = false;
      this.gethit = false;
      this.explode=[];
      this.destroytimer=0;
    }
    
    SetSpeed(speed)
    {
      this.pyspeed = speed;
    }

    SetGap(gap)
    {
      this.gap = gap;

    }
    
    reset()
    {
      this.posY = random(0,this.h/2);
      
      if (this.reverse==false)
      {
        this.x = 0;
      }
      else
      {
        this.x = this.w+100;
      }
      this.gethit = false;

      GM_INCREASET_POINT();
    }
    
    // Call with the current coordonate of the cube
    update(dir)
    {
      // 
      if (this.reverse==false)
      {
        this.x+=this.speed;
        if (this.x>this.w+25)
        this.reset();            
      }
      else
      {
        this.x-=this.speed;
        if (this.x<0)
        this.reset();            
      }
      
      if (this.explode.length > 0)
      {
        let finished=true;
        this.explode.forEach(p=>{
          p.update();
          if (p.lifetime>0)
          finished = false;
        })

        if (finished)
        this.explode=[];
      }

      if (this.gethit == true)
      return;
      
      this.py+=this.pyacc*this.pyspeed;

      if (this.py>this.h+25)
      this.py=this.h+25;

      if (this.py<=((this.h/2)+25))
      this.py=((this.h/2)+25);

      // Get Colission
      
      if ( (this.x+25>=this.w/2) && (this.x+25<(this.w/2+25)) ||
          (this.x>=this.w/2) && (this.x<(this.w/2+25)) )
      {
        if ( (this.posY>(this.py-this.h/2)) || (this.posY>(this.py-this.h/2-25)) ||
            (this.posY+this.gap<(this.py-this.h/2)) )
        {
          GM_HIT();
          this.gethit = true;
          for(let i=0;i<20;i++)
          this.explode.push(new Particle(this.sx+this.w/2,this.sy+this.py,255));
        }
      }


    }

    stop()
    {
      this.pyacc = 0;
    }
    moveup()
    {
      this.pyacc = 1;
    }
    
    movedown()
    {
      this.pyacc = -1;
    }
    
    draw()
    {
        
        stroke(0)
        fill(0)
        rect(this.sx+0,this.sy+this.h/2+25,this.w+this.gap ,this.h/2+this.gap )
        stroke(255)
        fill(255)
        rect(this.sx+this.x, this.sy+this.h/2+25, 25, this.posY);
        rect(this.sx+this.x, this.sy+this.h/2+25+this.posY+this.gap, 25, this.h/2-this.posY);
      
        if (this.gethit == false)
        {
          fill(255);
          rect(this.sx+this.w/2, this.sy+this.py, 25, 25);
        }

        if (this.explode.length > 0)
        {
          this.explode.forEach(p=>{
            p.draw();
          })
        }

    }
  }


class PillarY
  {
    constructor(x=0,y=0,w=300,h=300,speed=0.5)
    {
      this.sy = y;
      this.sx = x;
      this.y = y;
      this.x = x;
      this.px = w/2;
      this.pxspeed=3;
      this.pxacc=0;
      this.speed=speed;
      this.gap = 120;
      this.sw = w;
      this.sh = h;
      this.w = w-50;
      this.h = h-50;
      this.posX=Math.random()*(this.w-50)+25;
      this.reverse = false;
      this.gethit = false;
      this.explode=[];
      this.destroytimer=0;
    }
    
    SetSpeed(speed)
    {
      this.pxspeed = speed+1;
    }

    SetGap(gap)
    {
      this.gap = gap;


    }
    
    reset()
    {
      this.posX = random(0,this.w);
      
      if (this.reverse==false)
      {
        this.y = 0;
      }
      else
      {
        this.y = this.h;
      }
      this.gethit = false;

      GM_INCREASET_POINT();
    }

    stop()
    {
      this.pxacc = 0;
    }

    moveleft()
    {
      this.pxacc = -1;
    }
    
    moveright()
    {
      this.pxacc = 1;
    }

    
    update()
    {
      if (this.reverse==false)
      {
        this.y+=this.speed;
        if (this.y>this.h/2)
        this.reset();            
      }
      else
      {
        this.y-=this.speed;
        if (this.y<0)
        this.reset();            
      }

      if (this.explode.length > 0)
      {
        let finished=true;
        this.explode.forEach(p=>{
          p.update();
          if (p.lifetime>0)
          finished = false;
        })

        if (finished)
        this.explode=[];
      }

      if (this.gethit == true)
      return;

      // Cube move control
      this.px+=this.pxacc*this.pxspeed;

      // Limitation
      if (this.px>this.w+25)
      this.px=this.w+25;
      if (this.px<0)
      this.px=0;



      // Colision
      if (this.gethit == false)
      if ( (this.y+25>=this.h/4) && (this.y+25<(this.h/4+25)) ||
          (this.y>=this.h/4) && (this.y<(this.h/4+25)) )
      {

        // Collision
        if ( (this.posX>this.px) || (this.posX>(this.px+25)) ||
              (this.posX+this.gap<(this.px+25)) )
        {
          GM_HIT();
          this.gethit = true;
          for(let i=0;i<20;i++)
          this.explode.push(new Particle(this.sx+this.px,this.sy+this.h/4,0));
        }
        //console.log("Range")
      }
    }
    
    draw()
    {
        
        stroke(255)
        fill(255)
        rect(this.sx,this.sy,this.w+50,this.h/2+25)
        stroke(0)
        fill(0)
        rect(this.sx, this.sy+this.y, this.posX, 25);
        rect(this.sx+this.posX+this.gap, this.sy+this.y, this.w-this.posX, 25);
      
        if (this.gethit == false)
        {
        fill(0);
        rect(this.sx+this.px,this.sy+this.h/4, 25, 25);
        }
        
        if (this.explode.length > 0)
        {
          this.explode.forEach(p=>{
            p.draw();
          })
        }

    }
  }


