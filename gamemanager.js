
let x=100
let y=300;
let sy = 0;
let sx=1;
let Speed = 2

let SpiritIngame=[
    "ingameSP",
    "ingameSP1",
    "ingameSP2",
    "ingameSP3",
    "ingameSP4",
    "ingameSP5",
    "ingameSP6",
    "ingameSP7"
]


let FleshIngame=[
    "fleshgame",
    "fleshgame1",
    "fleshgame2",
    "fleshgame3",
    "fleshgame4",
    "fleshgame5",
    "fleshgame6",
    "fleshgame7"
]


class GameManager
{
    constructor(jsonobj)
    {
        this.infos = jsonobj;
        this.index=0;
        this.dlgindex = 0;
        this.currentmusic = null;

        this.sfx1=null;
        this.sfx2=null;
        this.sfx3=null;
        this.bg1=null;
        this.bg2=null;
        this.bg3=null;
        this.bg4=null;

        this.bg1volume=0;
        this.bg2volume=0;
        this.bg3volume=0;
        this.bg4volume=0;

        this.bg1volumeTarget=0;
        this.bg2volumeTarget=0;
        this.bg3volumeTarget=0;
        this.bg4volumeTarget=0;

        this.GameOver=0;

        this.spirit=1;

        this.spiritHP = 1.0;
        this.fleshHP = 1.0;

        this.currenthitlevel = 5/100;

        this.interaction = false;
        this.interactionMessage="";

        this.modecounter=Math.random()*(16*30)+(16*20);

        this.running = false;

        this.stage = [10,15,20];
        this.stageindex = 0;
        this.stagecount=1;
        this.winsp=0;
        this.winflesh=0;

        this.state="";
        this.sequence=ASSET_GetJSONFromName("sequence");
        this.sequencefinished=false;
        this.dialogcounter=(Math.random()*20+10)*32;

        

    }

    GetIncreaseStageCount()
    {
        // COmpute the point
        if (this.spiritHP>this.fleshHP)
        {
            this.winsp++;
        }
        else
        {
            this.winflesh++;
        }

        this.stagecount++;
        if (this.stagecount>this.stage[this.stageindex])
        {
            this.stageindex++;
            if (this.stageindex>=this.stage.length)
            {
                if (this.winflesh>=this.winsp)
                {
                    this.state = "GAMEOVER";
                    this.sequence=ASSET_GetJSONFromName("gameover")
                    new Message(this.sequence);    
                }
                else
                {
                    this.state = "GAMEOVER";
                    this.sequence=ASSET_GetJSONFromName("wingame")
                    new Message(this.sequence);    
                }
            }
            else
            {
//                this.sequence=ASSET_GetJSONFromName("ingame")
                this.stagecount = 0;
//                new Message(this.sequence);


/*                this.spiritHP = 1.25;
                this.fleshHP = 1.0;*/
                GM_SET_LEVEL(this.stageindex);
            }
        }
    }

    GetSpiritHit()
    {
        this.sfx1.play();
        // Spirit = 0 => Flesh mode => Fleh hp bar take damage
        if (this.spirit==0)
        {
            this.fleshHP -= this.currenthitlevel;

            if (this.fleshHP<=0)
            {
                this.state = "GAMEOVER";
                this.sequence=ASSET_GetJSONFromName("gameover")
                new Message(this.sequence);  
            }
        }
        else
        {
            this.spiritHP -= this.currenthitlevel;

            
            if (this.spiritHP<=0)
            {
                this.state = "GAMEOVER";
                this.sequence=ASSET_GetJSONFromName("gameover")
                new Message(this.sequence);  
            }
        }
    }

    Update()
    {

        

        switch(this.state)
        {


            case "GAMEOVER":
                {

                    if (DialogManager!= null)
                    {
                        DialogManager.update();
                    }
                    else
                    {
                
                        this.bg1.setVolume(0);
                        this.bg2.setVolume(0);
                        this.bg3.setVolume(0);
                        this.bg4.setVolume(0.75);
                        this.bg4.stop();
                        this.bg4.loop();
                        this.state = "OVERWORLD";
                        this.sequence=ASSET_GetJSONFromName("sequence");
                        new Message(this.sequence);
                    }
                }
                break;

            case "OVERWORLD":
                {

                    if (DialogManager!= null)
                    {
                        DialogManager.update();
                    }
                    else
                    {
                        this.stageindex=0;
                        this.bg1.setVolume(0.75);
                        this.bg2.setVolume(0);
                        this.bg3.setVolume(0);
                        this.bg4.stop();
                        this.bg1.stop();
                        this.bg2.stop();
                        this.bg3.stop();
                        this.bg1.loop();
                        this.bg2.loop();
                        this.bg3.loop();

                        this.spiritHP = 1.0;
                        this.fleshHP = 1.0;
                        this.stagecount = 0;

                        this.winsp=0;
                        this.winflesh=0;
                        
                        this.state = "INNERWORLD";
                        GM_SET_LEVEL(this.stageindex);
                        
                        this.sequence=ASSET_GetJSONFromName("introingame");
                        new Message(this.sequence);
                    }
                }
                break;

                case "INNERWORLD":
                    {
                        if (this.dialogcounter>0)
                        {
                            this.dialogcounter--;
                        }
                        else
                        {
                            this.dialogcounter=(Math.random()*20+10)*16;
                            
//                            if (this.spirit==0)
                            let nb = Math.floor(Math.random()*7);
                            try
                            {
                                if (this.spirit==1)
                                this.sequence=ASSET_GetJSONFromName(SpiritIngame[nb]);
                                else
                                this.sequence=ASSET_GetJSONFromName(FleshIngame[nb]);
                                new Message(this.sequence);
                            }
                            catch (e)
                            {
                                console.log(nb.toString())
                                console.error(e);
                            }

                        }

                        if (DialogManager!= null)
                        {
                            DialogManager.update();
                        }

                        this.bg1volume+=(this.bg1volumeTarget-this.bg1volume)/64;
                        this.bg2volume+=(this.bg2volumeTarget-this.bg2volume)/64;
                        this.bg3volume+=(this.bg3volumeTarget-this.bg3volume)/64;
                
                        this.bg1.setVolume(this.bg1volume);
                        this.bg2.setVolume(this.bg2volume);
                        this.bg3.setVolume(this.bg3volume);
                
                        //this.bg3volume=0;
                        //this.bg4volume=0;
                
                        /*if (this.infos.Scenes.length>0)
                        {
                            this.infos.Scenes[0].INTERACTION.forEach(msg=>{
                                
                            })
                        }*/
                
                        if (this.modecounter<=0)
                        {
                            this.spirit = 1-this.spirit;
                            this.modecounter=Math.random()*(40*60)+(30*60);
                        }
                        else
                            this.modecounter-=1;
                    }
                    break;
                }


    }

    DrawBackground()
    {
        let scx = 0.95;
        let scy = 0.5;
        scale(scx,scy);
        if (this.spirit==0)
        {
            this.bg2volumeTarget = 0;
            this.bg3volumeTarget = 0.75;
            image(this.puzzlebackground2,-85,-140);
        }
        else
        {
            this.bg2volumeTarget = 0.75;
            this.bg3volumeTarget = 0;
            image(this.puzzlebackground1,-85,-140);
        }
        scale(1/scx,1/scy);
        
    }

    MousePressed()
    {
        DialogManager.next();
    }

    NextDialog()
    {
        DialogManager.next();
    }

    Draw()
    {
        if (DialogManager!= null)
        {
            DialogManager.draw();
        }

        switch(this.state)
        {
            case "OVERWORLD":
                {
                    //this.sequence.draw();
                }
                break;
            case "INNERWORLD":
                {
                    // Spirit Gauge
                    // ==============================================
                    textSize(height / 14);
                    fill('black')
                    strokeWeight(0)
                    rect(25,210,550,20)
                    fill('blue')
                    rect(25,212,this.spiritHP*545,15)
                    //fill('white')
                    //text(Math.floor(this.spiritHP*100)+"%",400,14);

                    // Flesh Gauge
                    // ==============================================
                    fill('black')
                    rect(25,230,550,20)
                    fill('red')
                    rect(25,232,this.fleshHP*545,15)

                    // Test regarding the Overwork or innerworld
                    this.bg1volumeTarget = 0.5;
                }
                break;

            case "GAMEOVER":
                {
                    /*let scx = 0.75;
                    let scy = 0.75;
                    scale(scx,scy);
                    image(this.GameOver,0,0);
                    scale(1/scx,1/scy);*/
                    this.stageindex = 0;
                    this.stagecount=1;
                }
                break;

            case "OVERWORLD":
                {
                    image(this.GameOver,0,0);
                }
                break;
        }





        //Cursor
        image(this.mouseicon,mouseX,mouseY);
    }

    Start()
    {
        this.sfx1=ASSET_GetSoundFromName("SFX_Damage");
        this.sfx2=ASSET_GetSoundFromName("SFX_Dialogue");
        this.sfx3=ASSET_GetSoundFromName("SFX_Failure");
        this.sfx4=ASSET_GetSoundFromName("SFX_Success");
        this.bg1=ASSET_GetSoundFromName("layer1");
        this.bg2=ASSET_GetSoundFromName("layer2");
        this.bg3=ASSET_GetSoundFromName("layer3");
        this.bg4=ASSET_GetSoundFromName("overworld");

        this.bg1.setVolume(0);
        this.bg2.setVolume(0);
        this.bg3.setVolume(0);
        this.bg4.setVolume(0.75);
        this.bg1.loop();
        this.bg2.loop();
        this.bg3.loop();
        this.bg4.loop();

        this.puzzlebackground1=ASSET_GetImageFromName("backgroundfight1");
        this.puzzlebackground2=ASSET_GetImageFromName("backgroundfight2");
        this.GameOver = ASSET_GetImageFromName("pow");
        this.mouseicon = ASSET_GetImageFromName("mouse");

        this.running = true;
        this.state="OVERWORLD";
        new Message(this.sequence);
    }

    NextLevel()
    {
  
    }

    RestartAllGame(){
        history.go(0);
    }

    GoToMode(mode)
    {
        this.state = mode;
    }

    PlayDialogue(){

        this.sfx2.play();
    }

    StopDialogue()
    {
        this.sfx2.stop();

    }
}