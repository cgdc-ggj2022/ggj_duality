

let gamemanager;
let gameready=false;
  
let GameFont;

  function preload() {
    ASSET_INIT();
    GameFont = ASSET_GetFontFromName('font1');
    GameInfos = ASSET_GetJSONFromName('game');

  }

function setup() 
{
createCanvas(600, 800);
noCursor();
frameRate(60); 
textFont(GameFont);
textSize(height / 10);
textAlign(CENTER, CENTER);

gamemanager = new GameManager(GameInfos);

}

// Instantiate the bar
let wall = new PillarX(25,250,550,550);
let wall2 = new PillarY(25,250,550,550);

  function draw() 
  {
    background(0);

    if (gameready == false)
    {
      fill(255)
      text("Mr. BOSS",200,200);
      text("CLICK TO START",250,400);
      return;
    }
    else
    {

    }

    let time = millis();


    /*if (gamemanager.running)
    {


    }
    else*/
    {
      // Different Menu and Scene management
      // quick solution to manage scenes and sequence
      switch(gamemanager.state)
      {
        case "TITLE":
          {
            fill(255)
            text("Mr. BOSS",200,200);
            text("CLICK TO START",250,400);
          }
          break;

        // Begin with dialog only no puzzle
        case 'OVERWORLD':
          {
            gamemanager.Update();
            gamemanager.Draw();
          }
          break;
          case "INGAME":
            {
              gamemanager.DrawBackground();

              wall.update();
              wall.draw();
              wall2.update();
              wall2.draw();

              gamemanager.Update();
              gamemanager.Draw();
            }
            break;
        case 'INNERWORLD':
          {
            gamemanager.DrawBackground();

            wall.update();
            wall.draw();
            wall2.update();
            wall2.draw();

            gamemanager.Update();
            gamemanager.Draw();
          }
          break;


          case 'GAMEOVER':
            {
              gamemanager.DrawBackground();
              gamemanager.Update();
              gamemanager.Draw();
            }
            break;

      }
    }


  
  }
  
  function mousePressed() 
  {
    if ( (gamemanager != undefined) && (gameready==false) )
    {
      gamemanager.Start();
      gameready=true;

    }
    else
      gamemanager.MousePressed();

  }

  function keyPressed() 
  {
    
    if (keyIsDown(UP_ARROW)) 
    {
      wall.movedown()
      wall2.stop()
    }  
    else if (keyIsDown(DOWN_ARROW)) 
    {
      wall.moveup()
      wall2.stop()
    }
    else if (keyIsDown(LEFT_ARROW)) 
    {
      wall2.moveleft()
      wall.stop()
    }
    else if (keyIsDown(RIGHT_ARROW)) 
    {
      wall2.moveright()
      wall.stop()
    }
  }
  

  // GLOAL SERVICE FOR GAME manager
  function GM_HIT()
  {
    gamemanager.GetSpiritHit();
  }

    // GLOAL SERVICE FOR GAME manager
    function GM_INCREASET_POINT()
    {
      gamemanager.GetIncreaseStageCount();
    }

function GM_GOTO_INNERWORLD()
{
  gamemanager.GoToMode("INNERWORLD");
}


function GM_SET_LEVEL(level)
{
  switch(level)
  {
    case 0:
      {
        wall.SetSpeed(2);
        wall2.SetSpeed(2);
        wall.SetGap(120);
        wall2.SetGap(120);
      
      }
      break;

    case 1:
      {
        wall.SetSpeed(2);
        wall2.SetSpeed(2);
        wall.SetGap(75);
        wall2.SetGap(75);
      }
      break;

    case 2:
      {
        wall.SetSpeed(3);
        wall2.SetSpeed(3);
        wall.SetGap(60);
        wall2.SetGap(60);
      }
      break;
  }

}

function GM_PLAY_DIALOG(){
  gamemanager.PlayDialogue();
}

function GM_STOP_DIALOG(){
  gamemanager.StopDialogue();
}
